<?php

require_once 'index.php';

$sheep = new Animal("shaun");

echo $sheep->getName();
echo $sheep->getLegs();
echo $sheep->getColdBlooded();


require_once 'Frog.php';
require_once 'Ape.php';

$sungokong = new Ape("kera sakti");
$sungokong->yell();

$kodok = new Frog("buduk");
$kodok->jump();
